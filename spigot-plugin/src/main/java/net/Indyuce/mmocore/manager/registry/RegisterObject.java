package net.Indyuce.mmocore.manager.registry;

public interface RegisterObject {

    /**
     * Identifier used to register an object
     */
    public String getId();
}
